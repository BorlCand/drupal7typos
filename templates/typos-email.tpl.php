<!DOCTYPE html>
<html>
    <head>
    </head>
    <body>
        <div>
            <h2><? echo t('Typo Report') ?></h2>
            <p><? echo t('Reported text: '); ?><? echo $text;?></p>
            <p><? echo t('Field Name: '); ?><strong><? echo $field_name;?></strong></p>
            <p><? echo t('Node type: '); ?><strong><? echo $entity;?></strong></p>
            <p><? echo t('Look at: '); ?><strong><? echo l('take a look', $see_url); ?></strong></p>
            <p><? echo t('Edit at: '); ?><strong><? echo l('edit', $edit_url); ?></strong></p>
            <p><? echo t('Report timestamp: '); ?><strong><? echo date('Y-m-d H:i:s', $timestamp); ?></strong></p>
            <p><? echo t('User ID: ') ?><strong><? echo $uid;?></strong></p>
            <p><? echo t('User IP: ') ?><strong><? echo $hostname;?></strong></p>
            <p><? echo t('User comment: '); ?><strong><? echo $comment;?></strong></p>
        </div>
    </body>
</html>

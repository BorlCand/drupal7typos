<?php
/**
 * @file
 * Default theme implementation for the typos reports form.
 *
 * Available variables:
 * - $typos_report_form: typos report form made from typos_report_form().
 *
 * @see template_preprocess()
 * @see template_preprocess_typos_report()
 */
?>
<div id="typos-report-content">
  <div id="typos-report-message">
    <div id="typos-message">
      <div id="typos-title"></div>
      <div id="typos-context-div"></div>
<?php
      print t('Click "Send typos report" button to complete the report. You can also include a comment.');
?>
    </div>
    <div id="typos-form">
<?php
    print $typos_report_form;
?>
    </div>
  </div>
  <div id="typos-report-result" style="display: none;">
  </div>
</div>
<div id="tmp"></div>
<?php
/**
 * @file
 * Admin settings form for typos module
 */

module_load_include('inc', 'typos', 'typos.validation');

/**
 * Typo settings form.
 */
function typos_admin_settings()
{
  $form['typos_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Typos is active'),
    '#description' => t('Globally activate this plugin, otherwise it will only work with configured fields'),
    '#default_value' => variable_get('typos_active', 1),
  );
  
  $form['typos_reports_ttl'] = array(
    '#type' => 'select',
    '#title' => t('Delete old reports'),
    '#description' => t('Delete reports older than N days'),
    '#default_value' => variable_get('typos_reports_ttl', 10080),
    '#options' => array(
      1440 => t('1 day'),
      4320 => t('3 days'),
      10080 => t('1 week'),
      20160 => t('2 weeks'),
      43800 => t('1 month'),
      0 => t('Never'),
    ),
    '#required' => TRUE,
  );
  
  $form['typos_reports_max_char'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum characters in selection'),
    '#element_validate' => array('element_validate_integer_positive'),
    '#default_value' => variable_get('typos_reports_max_char', 20),
    '#required' => TRUE,
  );
  
  $form['typos_email'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail'),
    '#description' => t('E-mail, on which Typos will send reports. Default is admin e-mail'),
    '#default_value' => variable_get('typos_email', ''),
    '#element_validate' => array('element_validate_email_address_settings')
  );
  
  $form['typos_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Report limit per IP/day'),
    '#element_validate' => array('element_validate_integer_positive_or_zero'),
    '#default_value' => variable_get('typos_limit', 5),
    '#required' => TRUE,
  );
  
  $form['typos_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Popup text'),
    '#description' => t('Text which will be shown to user when reporting a typo'),
    '#default_value' => variable_get('typos_text', 'Thanks for reporting!'),
    '#required' => TRUE,
  );
  
  return system_settings_form($form);
}

function typos_view_reports()
{
  $header = array(
    array('data' => t('ID'), 'field' => 't.tid', 'sort' => 'DESC'),
    array('data' => t('Entity'), 'field' => 't.entity'),
    array('data' => t('Posted In'), 'field' => 't.timestamp'),
    array('data' => t('Field'), 'field' => 't.fid'),
    array('data' => t('Text'), 'field' => 't.text'),
    array('data' => t('Author'), 'field' => 't.uid'),
    array('data' => t('IP Address'), 'field' => 't.hostname'),
    array('data' => t('Actions')),
  );
  
  $caption = t('Reported typos');
  $rows = array();
  
  $query = db_select('typos', 't');
  $query->leftJoin('field_config', 'f', 'f.id = t.fid');
  $query
    ->fields('t')
    ->fields('f', array('field_name'))
    ->extend('PagerDefault')
    ->extend('TableSort')
    ->limit(20)
    ->orderByHeader($header);
  
  $field_info = field_info_instances();
  
  foreach ($query->execute()->fetchAll() as $item) {
    $rows[] = array(
        $item->tid,
        l($item->entity . '/' . $item->eid, $item->entity . '/' . $item->eid, array('attributes'=>array('target' => 'blank'))),
        format_date($item->timestamp, $type = 'short'),
        $item->fid ? $item->field_name . ' / ' . t($field_info[$item->entity][$item->bundle][$item->field_name]['label']) : t('N/A'),
        format_string($item->text),
        format_username(user_load($item->uid)),
        $item->hostname,
        l(t('edit'), $item->entity . '/' . $item->eid . '/edit', array('attributes' => array('target' => 'blank'))),
    );
  }
  
  return theme(
    'table',
    array(
      'header' => $header,
      'rows' => $rows,
      'caption' => $caption
    )
  );
}

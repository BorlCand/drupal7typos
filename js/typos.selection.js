(function ($) {

  Drupal.behaviors.typos_helper = {
    COUNT_SYMBOLS_AROUND: 60,

    /**
     * Function finds selected text.
     */
    typos_get_sel_text: function () {
      if (window.getSelection) {
        txt = window.getSelection();
        selected_text = txt.toString();
        full_text = txt.anchorNode.textContent;
        selection_start = txt.anchorOffset;
        selection_end = txt.focusOffset;

        element = $(txt.anchorNode).parents('.typos-field');
      }
      else if (document.getSelection) {
        txt = document.getSelection();
        selected_text = txt.toString();
        full_text = txt.anchorNode.textContent;
        selection_start = txt.anchorOffset;
        selection_end = txt.focusOffset;

        element = $(txt.anchorNode).parents('.typos');
      }
      else if (document.selection) {
        txt = document.selection.createRange();
        selected_text = txt.text;
        full_text = txt.parentElement().innerText;

        var stored_range = txt.duplicate();
        stored_range.moveToElementText(txt.parentElement());
        stored_range.setEndPoint('EndToEnd', txt);
        selection_start = stored_range.text.length - txt.text.length;
        selection_end = selection_start + selected_text.length;

        element = $(txt.parentElement()).parents('.typos');
      }
      else {
        return;
      }

      if (element.length === 0) {
        return;
      }

      var txt = {
        selected_text: selected_text,
        full_text: full_text,
        selection_start: selection_start,
        selection_end: selection_end,
        element: element[0]
      };

      return txt;
    },
    /**
     * Function gets a context of selected text.
     */
    typos_get_sel_context: function (sel) {
      selection_start = sel.selection_start;
      selection_end = sel.selection_end;
      if (selection_start > selection_end) {
        tmp = selection_start;
        selection_start = selection_end;
        selection_end = tmp;
      }

      var sel_context = sel.full_text;

      context_first = sel_context.substring(0, selection_start);
      context_second = '<strong>' + sel_context.substring(selection_start, selection_end) + '</strong>';
      context_third = sel_context.substring(selection_end, sel_context.length);
      sel_context = context_first + context_second + context_third;

      context_start = selection_start - this.COUNT_SYMBOLS_AROUND;
      if (context_start < 0) {
        context_start = 0;
      }

      context_end = selection_end + this.COUNT_SYMBOLS_AROUND;
      if (context_end > sel_context.length) {
        context_end = sel_context.length;
      }

      sel_context = sel_context.substring(context_start, context_end);

      context_start = sel_context.indexOf(' ') + 1;

      if (selection_start + this.COUNT_SYMBOLS_AROUND < sel_context.length) {
        context_end = sel_context.lastIndexOf(' ', selection_start + this.COUNT_SYMBOLS_AROUND);
      }
      else {
        context_end = sel_context.length;
      }

      selection_start = sel_context.indexOf('<strong>');
      if (context_start > selection_start) {
        context_start = 0;
      }

      if (context_start) {
        sel_context = sel_context.substring(context_start, context_end);
      }

      return sel_context;
    }
  };
})(jQuery);
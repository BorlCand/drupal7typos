(function ($) {

  Drupal.behaviors.typos = {
    attach: function (context, settings) {
      // Show typos report window on Ctrl + Enter.
      $(document).keydown(function (event) {
        if (event.ctrlKey && event.keyCode === 13) {
          $.fn.typos_report_window();
        }
      });

      // callback for Drupal ajax_command_invoke function
      $.fn.typos_js_callback = function (res) {
        $('#typos-report-message').hide();
        $('#typos-report-result').show().html(Drupal.t('Your message has been sent. Thank you.'));
        setTimeout(modalContentClose, 1000);
      };

      /**
       * Function restores typos report form if form was shown, but report was not sent.
       */
      function typos_restore_form() {
        if ($('#typos-report-result').is(':visible')) {
          $('#typos-report-content').appendTo('#typos-report-wrapper');
        }
      }

      /**
       * Function shows modal window.
       */
      $.fn.typos_report_window = function () {
        var sel = Drupal.behaviors.typos_helper.typos_get_sel_text();
        if (sel === undefined || sel.selected_text.length <= 0) {
        }
        else if (sel.selected_text.length > $(sel.element).data('max-length')) {
          // You can alert when user tries to report a typo reaching limit
          // alert(Drupal.t('No more than !max_chars characters can be selected when creating a typo report.', {'!max_chars': Drupal.settings.typos.max_chars}));
        }
        else {
          // Get selection sel_context.
          var sel_context = Drupal.behaviors.typos_helper.typos_get_sel_context(sel);

          // Show dialog.
          Drupal.CTools.Modal.show(Drupal.settings.TyposModal);
          $('#typos-modal-content').html('&nbsp;');
          $('#typos-report-content').appendTo('#typos-modal-content');

          $('#typos-title').html($(sel.element).data('popup-text'));
          $('#typos-context-div').html(sel_context);
          $('#typos-context').val(sel_context);
          $('#typos-url').val(window.location);
          $('#typos-eid').val($(sel.element).data('entity-id'));
          $('#typos-fid').val($(sel.element).data('field-id'));
          $('#typos-entity').val($(sel.element).data('entity'));
          $('#typos-bundle').val($(sel.element).data('bundle'));

          var typos_close, typos_click_close;

          // Close modal by Esc press.
          $(document).keydown(typos_close = function (e) {
            if (e.keyCode === 27) {
              typos_restore_form();
              modalContentClose();
              $(document).unbind('keydown', typos_close);
            }
          });

          // Close modal by clicking outside the window.
          $('#modalBackdrop').click(typos_click_close = function (e) {
            typos_restore_form();
            modalContentClose();
            $('#modalBackdrop').unbind('click', typos_click_close);
          });

          // Close modal by 'close' link click.
          $('#close').click(function (e) {
            typos_restore_form();
            modalContentClose();
            $(document).unbind('keydown', typos_close);
          });
        }
      };
    }
  };
})(jQuery);
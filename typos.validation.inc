<?php
/**
 * @file
 */


function element_validate_email_address($element, &$form_state)
{
  $value = trim($element['#value']);
  if ($value !== '' && !valid_email_address($value)) {
    form_error($element, t('Your email address is invalid. Please enter a valid address. Correct format is: "example@email.com"'));
  }
}

function element_validate_email_address_settings($element, &$form_state)
{
  $value = trim($element['#value']);
  
  if ($value === '') {
    form_set_value($element, variable_get('site_mail'), $form_state);
  } else {
    element_validate_email_address($element, $form_state);
  }
}

/**
 * Form element validation handler for integer elements that must be positive or 0.
 */
function element_validate_integer_positive_or_zero($element, &$form_state) {
  $value = $element['#value'];
  if ($value !== '' && (!is_numeric($value) || intval($value) != $value || $value < 0)) {
    form_error($element, t('%name must be a positive or 0 integer.', array('%name' => $element['#title'])));
  }
}